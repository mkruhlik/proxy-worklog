package com.polontech.jira.service;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by MK on 07.06.2016.
 */
public class ProxyConfigService {

    private static final String KEY_ROLE_LOGGING = "roleLogging";
    private static final String KEY_FOR_ROLE_LOGGING = "forRoleLogging";

    private static PropertySet ofbizwim;

    private static ProxyConfigService configService;

    /**
     *
     */
    private ProxyConfigService() {
        if (ofbizwim == null) {
            Map<String, Object> ofbizAgrs = new HashMap<String, Object>();
            ofbizAgrs.put("delegator.name", "default");
            ofbizAgrs.put("entityName", "com.polontech.jira.proxy-worklog");
            ofbizAgrs.put("entityId", 1l);
            ofbizwim = PropertySetManager.getInstance("ofbiz", ofbizAgrs);
        }
    }

    /**
     * Singleton implementation
     *
     * @return
     */
    public static ProxyConfigService getInstance() {
        if (configService == null) {
            configService = new ProxyConfigService();
        }

        return configService;
    }

    public Long getRoleLogging() {
        return ofbizwim.getLong(KEY_ROLE_LOGGING);
    }

    public void setRoleLogging(@Nonnull Long roleLogging) {
        ofbizwim.setLong(KEY_ROLE_LOGGING, roleLogging);
    }

    public Long getForRoleLogging() {
        return ofbizwim.getLong(KEY_FOR_ROLE_LOGGING);
    }

    public void setForRoleLogging(@Nonnull Long forRoleLogging) {
        ofbizwim.setLong(KEY_FOR_ROLE_LOGGING, forRoleLogging);
    }
}

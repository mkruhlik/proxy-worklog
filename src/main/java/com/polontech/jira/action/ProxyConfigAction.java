package com.polontech.jira.action;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.polontech.jira.service.ProxyConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 *
 */
public class ProxyConfigAction extends JiraWebActionSupport {

    private static final Logger log = LoggerFactory.getLogger(ProxyConfigAction.class);

    private Long roleLogging;
    private Long forRoleLogging;

    /**
     * Saving the configuration data and redirect to the configuration page
     *
     * @return
     * @throws Exception
     */
    public String doSaveConfig() throws Exception {
        ProxyConfigService configService = ProxyConfigService.getInstance();

        configService.setRoleLogging(roleLogging);
        configService.setForRoleLogging(forRoleLogging);

        return getRedirect("/secure/admin/ProxyPluginConfig.jspa");
    }

    public Collection<ProjectRole> getProjectRoles() {
        ProjectRoleManager projectRoleManager = ComponentAccessor.getComponentOfType(ProjectRoleManager.class);
        return projectRoleManager.getProjectRoles();
    }

    public Long getRoleLogging() {
        return ProxyConfigService.getInstance().getRoleLogging();
    }

    public void setRoleLogging(Long roleLogging) {
        this.roleLogging = roleLogging;
    }

    public Long getForRoleLogging() {
        return ProxyConfigService.getInstance().getForRoleLogging();
    }

    public void setForRoleLogging(Long forRoleLogging) {
        this.forRoleLogging = forRoleLogging;
    }
}

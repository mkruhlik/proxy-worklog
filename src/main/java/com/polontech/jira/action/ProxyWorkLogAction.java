package com.polontech.jira.action;

import com.atlassian.extras.common.log.Logger;
import com.atlassian.extras.common.log.Logger.Log;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class ProxyWorkLogAction extends JiraWebActionSupport {
    /**
     * The log serves to write out the description of the errors/ exception /
     * warnings/ debug for particular class in the JIRA log file .
     */
    private static final Log log = Logger.getInstance(ProxyWorkLogAction.class);


    private Long issueId;

    /*
     * (non-Javadoc)
     *
     * @see webwork.action.ActionSupport#doExecute() Once the user clicks create
     * Bug/Test Case this methods starts execute action of displaying create
     * issue dialog. Some fields (issue type, reporter, assignee, priority)
     * should be preselected in order to create the Test Case or Bug for
     * Requirement.
     */
    @Override
    protected String doExecute() throws JSONException {
        return SUCCESS;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }
}

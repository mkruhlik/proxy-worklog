package com.polontech.jira.condition;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Preconditions;
import com.polontech.jira.service.ProxyConfigService;

/**
 * Created by MK on 08.06.2016.
 */
public class UserIsInProjectRoleCondition extends AbstractWebCondition {
    private final ProjectRoleManager projectRoleManager;

    public UserIsInProjectRoleCondition(ProjectRoleManager projectRoleManager) {
        this.projectRoleManager = Preconditions.checkNotNull(projectRoleManager);
    }

    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        boolean isIn = false;

        Long projectRoleId = ProxyConfigService.getInstance().getRoleLogging();
        ProjectRole projectRole = projectRoleId == null ? null : projectRoleManager.getProjectRole(projectRoleId);
        if (projectRole != null
                && projectRoleManager.isUserInProjectRole(applicationUser, projectRole, jiraHelper.getProjectObject())) {
            isIn = true;
        }

        return isIn;
    }
}

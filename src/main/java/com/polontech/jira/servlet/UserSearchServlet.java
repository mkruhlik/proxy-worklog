package com.polontech.jira.servlet;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.extras.common.log.Logger;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.base.Preconditions;
import com.polontech.jira.service.ProxyConfigService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by MK on 07.06.2016.
 */
public class UserSearchServlet extends HttpServlet {

    private static final Logger.Log log = Logger.getInstance(UserSearchServlet.class);

    private static final String PARAMETER_ISSUE_ID = "issueId";
    private static final String PARAMETER_TERM = "term";

    private final UserManager userManager;
    private final UserPickerSearchService userSearchService;
    private final IssueManager issueManager;
    private final ProjectRoleManager projectRoleManager;

    public UserSearchServlet(UserManager userManager,
                             UserPickerSearchService userSearchService,
                             IssueManager issueManager,
                             ProjectRoleManager projectRoleManager) {
        this.userManager = Preconditions.checkNotNull(userManager);
        this.userSearchService = Preconditions.checkNotNull(userSearchService);
        this.issueManager = Preconditions.checkNotNull(issueManager);
        this.projectRoleManager = Preconditions.checkNotNull(projectRoleManager);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String issueId = req.getParameter(PARAMETER_ISSUE_ID);

        if (StringUtils.isBlank(issueId) || !StringUtils.isNumeric(issueId)) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Long projectRoleId = ProxyConfigService.getInstance().getForRoleLogging();
        if (projectRoleId == null) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            log.error("Plugin is not configured");

            return;
        }
        ProjectRole projectRole = projectRoleManager.getProjectRole(projectRoleId);
        MutableIssue issue = issueManager.getIssueObject(Long.parseLong(issueId));

        if (projectRole == null
                || issue == null) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            if (projectRole == null) {
                log.error("Project role with the ID[" + projectRoleId + "] does not exist");
            }

            if (issue == null) {
                log.error("Issue with the ID[" + issueId + "] does not exist");
            }

            return;
        }


        JSONObject jsonObject = new JSONObject();
        try {
            String term = req.getParameter(PARAMETER_TERM);

            JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
            JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(authenticationContext.getUser());
            List<User> users = userSearchService.findUsers(jiraServiceContext, term);

            JSONArray arUser = new JSONArray();
            JSONObject objUser = null;
            ApplicationUser applicationUser = null;
            if (!users.isEmpty()) {
                for (User userItem : users) {
                    String userKey = ComponentAccessor.getUserKeyService().getKeyForUser(userItem);
                    applicationUser = userManager.getUserByKey(userKey);
                    if (projectRoleManager.isUserInProjectRole(applicationUser, projectRole, issue.getProjectObject())) {
                        if (applicationUser != null) {
                            objUser = new JSONObject();
                            objUser.put("name", userItem.getName());
                            objUser.put("displayName", userItem.getDisplayName());
                            objUser.put("email", userItem.getEmailAddress());
                            objUser.put("avatar", ComponentAccessor.getAvatarService().getAvatarUrlNoPermCheck(applicationUser, Avatar.Size.SMALL));

                            arUser.put(objUser);
                        }
                    }
                }
            }

            jsonObject.put("users", arUser);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        resp.setContentType("application/json; charset=\"utf-8\"");
        PrintWriter writer = resp.getWriter();
        writer.print(jsonObject.toString());
        writer.flush();
    }
}

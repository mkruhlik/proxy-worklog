package com.polontech.jira.servlet;

import com.atlassian.extras.common.log.Logger;
import com.atlassian.extras.common.log.Logger.Log;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddWorklogServlet extends HttpServlet {

    private static final Log log = Logger.getInstance(AddWorklogServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            doService(req, resp);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            doService(req, resp);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void doService(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, JSONException {
        JSONObject result = new JSONObject();
        String userId = req
                .getParameter("userId");
        String time = req
                .getParameter("time");
        String date = req
                .getParameter("date");
        String comment = req
                .getParameter("comment");
        String issueKey = req
                .getParameter("issueKey");

        log.error(issueKey + "issueKey");
        log.error(userId + "userId");

        log.error(time + "time");

        log.error(date + "date");

        log.error(comment + "comment");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateF = new Date();
        try {
            dateF = df.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.error(dateF + "dateF");

        IssueManager issueManager = ComponentAccessor.getIssueManager();
        MutableIssue mI = issueManager.getIssueObject(issueKey);

        UserUtil userUtil = ComponentAccessor.getUserUtil();
        ApplicationUser user = userUtil.getUserByName(userId);
        WorklogManager worklogManager = ComponentAccessor.getWorklogManager();

        Worklog newWorklog = new WorklogImpl(worklogManager, mI, null, user.getUsername(), comment, dateF, null, null, Long.parseLong(time) * 60 * 60);

        Worklog newlyCreatedWorklog = worklogManager.create(user.getDirectoryUser(), newWorklog, mI.getEstimate(), false);
        log.error(newlyCreatedWorklog.getIssue().getKey());
        PrintWriter out = resp.getWriter();
        resp.setContentType("plain/text; charset=\"utf-8\"");
        out.write(result.toString());
    }
}
